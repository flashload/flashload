from datetime import datetime
import string
import random


def is_different():
    with open('../FlashLoad/FlashLoad.yaml', 'r') as file:
        old = file.read()

    with open('../.config/clash/subscribe.yaml', 'r') as file:
        new = file.read()

    if old == new:
        print('No change')
    else:
        print('Update')


def get_server_lists():
    '''
    Get server lists from the subscribe.yaml
    '''

    with open('../.config/clash/subscribe.yaml', 'r') as file:
        subs = file.readlines()

    proxy = '    - { name: \'🌶️ SP\', type: ss, server: ignore.this, port: 443, cipher: aes-128-gcm, password: ignore-this, udp: true }\n'
    subs.insert(subs.index('proxy-groups:\n'), proxy)

    proxy_group = '    - { name: Powered By, type: select, proxies: [\'🌶️ SP\'] }\n'
    subs.insert(subs.index('rules:\n'), proxy_group)

    return subs


def read_csv():
    with open('../customers_list.csv', 'r') as file:
        valid_lst = [entry.strip()
                     for entry in file.readlines() if 'Valid' in entry]
        return valid_lst


def update_expired_csv():
    with open('../customers_list.csv', 'r') as file:
        lst = [entry.strip() for entry in file.readlines()]

    new_lst = list()
    for entry in lst:
        if 'Valid' in entry:
            entry_lst = entry.split('; ')
            dt = datetime.strptime(entry_lst[2].replace('\"', ''), "%b %d, %Y")
            if dt.timestamp() - datetime.now().timestamp() < 0:
                entry_lst[3] = 'Expired'
            new_lst.append('; '.join(entry_lst))
        else:
            new_lst.append(entry)

    with open('../customers_list.csv', 'w') as file:
        file.write('\n'.join(new_lst) + '\n')


def write_customer_files(code, date, servers):

    customer_servers = servers.copy()

    date_part = "    - { name: \'" + date + \
        "\', type: ss, server: indicate.date, port: 443, cipher: aes-128-gcm, password: indicate.date, udp: true }\n"
    customer_servers.insert(
        customer_servers.index('proxies:\n') + 1, date_part)

    date_group = "    - { name: 到期日期, type: select, proxies: [" + date + "] }\n"
    customer_servers.insert(customer_servers.index(
        'proxy-groups:\n') + 1, date_group)

    out = ''.join(customer_servers)
    out = out.replace('DuangCloud', 'FlashLoad')

    file_dir = '../FlashLoad/Subs/{}.yaml'.format(code)
    with open(file_dir, 'w') as file:
        file.write(out)


def write_default_files(servers):

    customer_servers = servers.copy()

    out = ''.join(customer_servers)
    out = out.replace('DuangCloud', 'FlashLoad')

    file_dir = '../FlashLoad/FlashLoad.yaml'
    with open(file_dir, 'w') as file:
        file.write(out)


def write_npy_files(servers):

    customer_servers = servers.copy()

    france = "    - {name: \'" + "🇫🇷 法国专线 1" + "\', server: qzcm.tagsubs.xyz, port: 8824, type: ssr, cipher: aes-256-cfb, password: V2efLnR3SJZr, protocol: auth_aes128_sha1, obfs: http_simple, protocol-param: 297395:v6gOEF, obfs-param: 268e5297395.microsoft.com }\n"

    customer_servers.insert(customer_servers.index('proxy-groups:\n'), france)
    
    proxies = customer_servers[customer_servers.index('proxy-groups:\n') + 1]
    proxies = proxies.replace("] }", ", \'🇫🇷 法国专线 1\'] }")
    customer_servers[customer_servers.index('proxy-groups:\n') + 1] = proxies

    nj = "    - { name: \'" + "🐷 彭哥最爱的那姐" + \
        "\', type: ss, server: indicate.date, port: 443, cipher: aes-128-gcm, password: indicate.date, udp: true }\n"
    customer_servers.insert(customer_servers.index('proxy-groups:\n'), nj)

    nj_group = "    - { name: Only For, type: select, proxies: [" + "🐷 彭哥最爱的那姐" + "] }\n"
    customer_servers.insert(customer_servers.index('rules:\n'), nj_group)

    out = ''.join(customer_servers)
    out = out.replace('DuangCloud', 'FlashLoad')
    out = out.replace('🌶️ SP', '🚡 那姐的彭哥')

    file_dir = '../FlashLoad/FlashLoad_NJ.yaml'
    with open(file_dir, 'w') as file:
        file.write(out)


def update():
    is_different()
    update_expired_csv()
    current_servers = get_server_lists()
    current_users = read_csv()

    write_default_files(current_servers)
    write_npy_files(current_servers)

    for users in current_users:
        entry = users.split('; ')
        dt = datetime.strptime(entry[2].replace('\"', ''), "%b %d, %Y")
        write_customer_files(
            entry[-1], dt.strftime("%Y-%m-%d"), current_servers)


def add():
    import pyperclip as clip
    if '\t' in clip.paste():
        entry = clip.paste().split('\t')
        entry.append('Valid')
        random_code = ''.join(random.choice(string.ascii_uppercase)
                              for _ in range(8))
        entry.append(random_code)
        new_entry = '; '.join(entry) + '\n'
        with open('../customers_list.csv', 'a') as file:
            file.write(new_entry)
        update()
        link = 'https://gitlab.com/flashload/flashload/-/raw/master/Subs/{}.yaml'.format(
            random_code)
        clip.copy(link)


update()
